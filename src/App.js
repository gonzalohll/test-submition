import React from "react";
import CascadeTest from "./components/CascadeTest"
function App() {
  const data = {Germany: 'Berlin', Argentina: 'Buenos Aires', USA: 'Washington', France: 'Paris', Poland: 'Warsaw', Australia: 'Canberra', China: 'Beijing', India: 'New Delhi'};
  return (
    <CascadeTest data={data} />
  );
}

export default App;
