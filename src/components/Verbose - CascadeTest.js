import React, {useState, useEffect} from "react";

const CountryCapitalGame = () => {
 
    //I set the data that would insted be a prop.
    const data = {Germany: 'Berlin', Argentina: 'Buenos Aires'}

    const [firstSelection, setFirstSelection] = useState('');

    const [randomizedCountryCapital, setRandomizedCountryCapital] = useState([]);
    const [buttonColors, setButtonColors] = useState({});

    //As the component mounts, I set randomize countries and capitals to then be displayed as buttons in the DOM.
    useEffect(() => {

        //I push the countries and capitals into an array
        const randomCountries = Object.keys(data)
        const randomCapitals = Object.values(data)
        
        /* 
            I then shuffle the array and store it in a state
            I use states so that the dom re renders when the array changes.
         */
        let temporalRandomizedCountryCapital = [...randomCountries, ...randomCapitals].map( (_, i, tempArrCopy) => {
            var rand = i + ( Math.floor( Math.random() * (tempArrCopy.length - i) ) );
            [tempArrCopy[rand], tempArrCopy[i]] = [tempArrCopy[i], tempArrCopy[rand]]
            return tempArrCopy[i]
        })

        setRandomizedCountryCapital([...temporalRandomizedCountryCapital]);
        /* 
            I set a default color for the buttons as a state. Again, so that the dom re renders when the state changes.
            Setting values in an object or array is faster to read and write, than making an evaluating conditions in a function,
            because it points to a specific place in memory.
         */
        let temporalButtonColors = {};
        temporalRandomizedCountryCapital.map((place)=>{
            temporalButtonColors={...temporalButtonColors, [place]: {backgroundColor:''}};
        })
        setButtonColors({...temporalButtonColors});
    }, [])

    //I prepare a function that sets the color for each button.
    const colorize = (targetValue,color) => {
        /* 
            I use a temporary variable, because setting states is asynchronous. 
            This way I can set multiple button colors at the same time avoiding collisions with unexpected results.
         */
        let tempButtonColors = {...buttonColors};
        targetValue.map((value)=>{
            tempButtonColors[value] = {backgroundColor:color};
        })
        
        //Now, I set every other button to the default color (none).
        for (let key in tempButtonColors) {
            if(targetValue.indexOf(key) < 0){
                tempButtonColors[key] = {backgroundColor:''};
            }
        }

        setButtonColors({
            ...tempButtonColors
        });
    }

    //This is the main buttons click handler.
    const handleClick = (targetValue) => {
        if(firstSelection?.length < 1){
            //If the first selection is empty, I set it to the clicked button, as well as it's color.
            colorize([targetValue],'#0000ff');
            setFirstSelection(targetValue);
        }else{
            checkIfWon(targetValue);
        }
    }

    //This function deletes the selections from the array used to display the buttons.
    const buttonsCleanUp = (secondSelection) => {
        for (let i = randomizedCountryCapital.length; i >=0; i--){
            if(randomizedCountryCapital[i]===firstSelection || randomizedCountryCapital[i]===secondSelection){
                randomizedCountryCapital.splice(i,1);
            }
        }
    }

    //This function checks if the user has won by comparing the two selections and the data.
    const checkIfWon =  (secondSelection) => {
        if(data[firstSelection]===secondSelection || data[secondSelection]===firstSelection){
            buttonsCleanUp(secondSelection);
            setFirstSelection('');
        } else {
            //In case the user has not won, I set the colors of the pressed buttons to red.
            colorize([firstSelection,secondSelection],'#ff0000');
            setFirstSelection('');
        }
    }


    return (
        <div>
            {
                randomizedCountryCapital.length ?
                randomizedCountryCapital.map((item, index) => {
                    return(
                        <button onClick={(e)=>handleClick(e.target.value)} value={item} key={index} style={buttonColors[item]}>
                            {item}
                        </button>
                    )
                })
                :
                <p>Contratulations.</p>
            }    
        </div>
    );
}

export default CountryCapitalGame;