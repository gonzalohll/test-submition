import React, {useState, useEffect} from "react";

const CascadeTest = ({data}) => {

    const [firstSelection, setFirstSelection] = useState('');

    const [randomizedCountryCapital, setRandomizedCountryCapital] = useState([]);
    const [buttonColors, setButtonColors] = useState({});

    useEffect(() => {
        const randomCountries = Object.keys(data)
        const randomCapitals = Object.values(data)

        let temporalRandomizedCountryCapital = [...randomCountries, ...randomCapitals].map( (_, i, arrCopy) => {
            var rand = i + ( Math.floor( Math.random() * (arrCopy.length - i) ) );
            [arrCopy[rand], arrCopy[i]] = [arrCopy[i], arrCopy[rand]]
            return arrCopy[i]
        })

        setRandomizedCountryCapital([...temporalRandomizedCountryCapital]);
        
        let temporalButtonColors = {};
        temporalRandomizedCountryCapital.map((place)=>{
            temporalButtonColors={...temporalButtonColors, [place]: {backgroundColor:''}};
        })
        setButtonColors({...temporalButtonColors});
    }, [])

    const colorize = (targetValue,color) => {
        if(targetValue.length <= 0){
            return;
        }else{
            let tempButtonColors = {...buttonColors};
            targetValue.map((value)=>{
                tempButtonColors[value] = {backgroundColor:color};
            })
            
            for (let key in tempButtonColors) {
                if(targetValue.indexOf(key) < 0){
                    tempButtonColors[key] = {backgroundColor:''};
                }
            }

            setButtonColors({
                ...tempButtonColors
            });
        }
    }

    const handleClick = (targetValue) => {
        
        if(firstSelection?.length < 1){
            colorize([targetValue],'#0000ff');
            setFirstSelection(targetValue);
        }else{
            checkIfWon(targetValue);
        }
    }

    const buttonsCleanUp = (secondSelection) => {
        for (let i = randomizedCountryCapital.length; i >=0; i--){
            if(randomizedCountryCapital[i]===firstSelection || randomizedCountryCapital[i]===secondSelection){
                randomizedCountryCapital.splice(i,1);
            }
        }
    }

    const checkIfWon =  (secondSelection) => {
        if(data[firstSelection]===secondSelection || data[secondSelection]===firstSelection){
            buttonsCleanUp(secondSelection);
            setFirstSelection('');
        } else {
            colorize([firstSelection,secondSelection],'#ff0000');
            setFirstSelection('');
        }
    }


    return (
        <div>
            {
                randomizedCountryCapital.length ?
                randomizedCountryCapital.map((item, index) => {
                    return(
                        <button onClick={(e)=>handleClick(e.target.value)} value={item} key={index} style={buttonColors[item]}>
                            {item}
                        </button>
                    )
                })
                :
                <p>Contratulations.</p>
            }    
        </div>
    );
}

export default CascadeTest;